# ProjectSummary
本Repo仅集中展示作者的私人项目及公开项目相关信息。
---
## *机场风切变预警项目*
- 机场全局预警
![机场全局预警](https://github.com/TheWrangler/ProjectSummary/blob/master/images/WindAlert/1.png)
- PPI显示
![PPI图](https://github.com/TheWrangler/ProjectSummary/blob/master/images/WindAlert/2.png)
- 下滑道显示
![下滑道图](https://github.com/TheWrangler/ProjectSummary/blob/master/images/WindAlert/3.png)
- 廓线显示
![廓线图](https://github.com/TheWrangler/ProjectSummary/blob/master/images/WindAlert/4.png)
